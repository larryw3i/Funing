# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# larry <larryw3i@163.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: Funing 0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-06-25 22:56+0800\n"
"PO-Revision-Date: 2021-12-06 23:35+0800\n"
"Last-Translator: larry <larryw3i@163.com>\n"
"Language-Team: Chinese (Simplified) <larryw3i@163.com>\n"
"Language: zh_CHS\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"X-Generator: Gtranslator 40.0\n"

#: funing/settings4t.py:10
msgid "funing"
msgstr ""

#: funing/settings4t.py:11
msgid "0.2.35"
msgstr ""

#: funing/settings4t.py:12 funing/settings4t.py:11
msgid "A face recognition gui"
msgstr ""

#: funing/.lwcp/fui/main_ui.py:20 funing/.lwcp/_fui/_main_ui.py:121
#: funing/.lwcp/ui/main_ui.py:20
msgid "Funing"
msgstr ""

#: funing/.lwcp/fui/main_ui.py:47 funing/.lwcp/ui/c_ui.py:19
#: funing/.lwcp/ui/main_ui.py:76
msgid "GO"
msgstr ""

#: funing/.lwcp/fui/main_ui.py:49 funing/.lwcp/ui/c_ui.py:21
#: funing/.lwcp/ui/main_ui.py:78
msgid "File"
msgstr ""

#: funing/.lwcp/fui/main_ui.py:49 funing/.lwcp/ui/c_ui.py:21
#: funing/.lwcp/ui/main_ui.py:78
msgid "Camera"
msgstr ""

#: funing/.lwcp/fui/main_ui.py:50 funing/.lwcp/ui/c_ui.py:22
#: funing/.lwcp/ui/main_ui.py:79
msgid "Open"
msgstr ""

#: funing/.lwcp/fui/main_ui.py:56 funing/.lwcp/ui/c_ui.py:30
#: funing/.lwcp/ui/main_ui.py:85
msgid "tolerance"
msgstr "tolerance(0.1-0.6)"

#: funing/.lwcp/fui/main_ui.py:63 funing/.lwcp/_fui/_main_ui.py:189
#: funing/.lwcp/ui/c_ui.py:37 funing/.lwcp/ui/main_ui.py:92
msgid "Pause"
msgstr ""

#: funing/.lwcp/fui/main_ui.py:66
msgid "Recognize"
msgstr ""

#: funing/.lwcp/fui/main_ui.py:69 funing/.lwcp/ui/c_ui.py:40
#: funing/.lwcp/ui/main_ui.py:95
msgid "Pick"
msgstr "Pick"

#: funing/.lwcp/fui/main_ui.py:110
msgid "Write it with certain rules so that you can analyze it later"
msgstr ""
"Write it in toml or yaml format, so it's a lot easier for you to analyze it."

#: funing/.lwcp/fui/main_ui.py:114 funing/.lwcp/ui/c_ui.py:55
#: funing/.lwcp/ui/main_ui.py:126
msgid "Save"
msgstr ""

#: funing/.lwcp/fui/main_ui.py:137 funing/.lwcp/_fui/_main_ui.py:119
msgid "About Funing"
msgstr ""

#: funing/.lwcp/_fui/_main_ui.py:85
msgid "No desktop environment is detected! (^_^)"
msgstr ""

#: funing/.lwcp/_fui/_main_ui.py:135
msgid "Licensed under the MIT license"
msgstr ""

#: funing/.lwcp/_fui/_main_ui.py:195
msgid "Play"
msgstr "Play"

#: funing/.lwcp/_fui/_main_ui.py:319
msgid "Select a file"
msgstr ""

#: funing/.lwcp/_fui/_main_ui.py:322
msgid "Image or video"
msgstr ""

#: funing/.lwcp/_fui/_main_ui.py:465 funing/.lwcp/_fui/_main_ui.py:722
msgid "Unable to open video source"
msgstr ""

#: funing/.lwcp/_fui/_main_ui.py:614
msgid "No informations found"
msgstr ""

#: funing/.lwcp/_fui/_main_ui.py:713
msgid "Restart Funing Now?"
msgstr ""

#: funing/.lwcp/_fui/_main_ui.py:728
msgid "Nothing enter"
msgstr "Nothing enter"

#: funing/.lwcp/_fui/_main_ui.py:729
#, fuzzy
msgid "You haven't entered anything yet! "
msgstr "You haven't entered data yet! Open a source and click 'Pick' to enter"

#: funing/.lwcp/ui/c_ui.py:48 funing/.lwcp/ui/main_ui.py:119
#, fuzzy
msgid "prev_symb"
msgstr "prev"

#: funing/.lwcp/ui/c_ui.py:50 funing/.lwcp/ui/main_ui.py:121
#, fuzzy
msgid "next_symb"
msgstr "next"

#: funing/settings4t.txt.py:10
msgid "@app_name"
msgstr ""

#: funing/settings4t.txt.py:11
msgid "@app_description"
msgstr ""

#~ msgid "show_per_page_str0"
#~ msgstr "show"

#~ msgid "10"
#~ msgstr "10"

#~ msgid "show_per_page_str1"
#~ msgstr "/page"

#, fuzzy
#~ msgid ""
#~ "Write it with certain rules sddddddddddddddddddddddddddddddddddddddddddo "
#~ "that you can analyze it later"
#~ msgstr ""
#~ "Write it in toml or yaml format, so it's a lot easier for you to analyze "
#~ "it."
